import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import BottomNavbar from './BottomNavbar';
import LoginScreen from './screen/LoginScreen';
import RegisterScreen from './screen/RegisterScreen';
import TransactionScreen from './screen/TransactionScreen';
import ProfileScreen from './screen/ProfileScreen';
import Detail from './screen/Detail';
import FormPemesanan from './screen/FormPemesanan';
import Keranjang from './screen/Keranjang';
import Summary from './screen/Summary';
import Confirmation from './screen/Confirmation';
import KodeReservasi from './screen/KodeReservasi';
import EditProfile from './screen/EditProfile';
import FAQ from './screen/FAQ';

const Stack = createNativeStackNavigator();

export default function AuthNavigation() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="LoginScreen" component={LoginScreen} />
      <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
      <Stack.Screen name="HomeScreen" component={BottomNavbar} />
      <Stack.Screen name="ProfilScreen" component={ProfileScreen} />
      <Stack.Screen name="Detail" component={Detail} />
      <Stack.Screen name="FormPemesanan" component={FormPemesanan} />
      <Stack.Screen name="Keranjang" component={Keranjang} />
      <Stack.Screen name="Summary" component={Summary} />
      <Stack.Screen name="Confirmation" component={Confirmation} />
      <Stack.Screen name="KodeReservasi" component={KodeReservasi} />
      <Stack.Screen name="EditProfile" component={EditProfile} />
      <Stack.Screen name="FAQ" component={FAQ} />
    </Stack.Navigator>
  );
}
