import {
  KeyboardAvoidingView,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useState} from 'react';
import Icon from 'react-native-vector-icons/dist/Ionicons';
import {CheckBox} from 'react-native-elements';

const FormPemesanan = ({navigation}) => {
  const [isSelected, setSelection] = useState(false);
  const [isChecked, setCheck] = useState(false);

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Icon
          name="arrow-back"
          size={22}
          color="black"
          onPress={() => navigation.goBack()}
        />
        <Text
          style={{
            color: 'black',
            marginLeft: 5,
            fontSize: 18,
            fontWeight: 'bold',
          }}>
          Formulir Pemesanan
        </Text>
      </View>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 20}}>
        <KeyboardAvoidingView enable keyboardVerticalOffset={-500}>
          <View style={{paddingHorizontal: 22}}>
            <Text style={styles.tittleInput}>Merek</Text>
            <TextInput
              placeholder="Masukan Merk Barang"
              style={styles.textInput}
            />
            <Text style={styles.tittleInput}>Warna</Text>
            <TextInput
              placeholder="Warna Barang, cth : Merah - Putih "
              style={styles.textInput}
            />
            <Text style={styles.tittleInput}>Ukuran</Text>
            <TextInput
              placeholder="Cth : S, M, L / 39,40"
              style={styles.textInput}
            />
            <Text style={styles.tittleInput}>Photo</Text>
            <TouchableOpacity style={styles.inputImg}>
              <Icon
                name="ios-camera-outline"
                size={22}
                color="#BB2427"
                style={{marginTop: 20}}
              />
              <Text style={{color: '#BB2427', paddingTop: 10}}>Add Photo</Text>
            </TouchableOpacity>
            <CheckBox
              title="Ganti Sol Sepatu"
              checked={isSelected}
              onPress={() => setSelection(!isSelected)}
              checkedColor={'#BB2427'}
              // style={styles.checkBox}
            />
            <CheckBox
              title="Jahit Sepatu"
              checked={isChecked}
              onPress={() => setCheck(!isChecked)}
              checkedColor={'#BB2427'}
              // style={styles.checkBox}
            />
            <CheckBox
              title="Repaint Sepatu"
              checked={isSelected}
              onPress={() => setSelection(!isSelected)}
              checkedColor={'#BB2427'}
              // style={styles.checkBox}
            />
            <CheckBox
              title="Cuci Sepatu"
              checked={isSelected}
              onPress={() => setSelection(!isSelected)}
              checkedColor="#BB2427"
              // style={styles.checkBox}
            />
            <Text style={styles.tittleInput}>Catatan</Text>
            <TextInput
              placeholder="Cth : ingin ganti sol baru"
              style={styles.catatan}
            />
            <TouchableOpacity
              style={styles.bottomconfirm}
              onPress={() => navigation.navigate('Keranjang')}>
              <Text style={styles.textbottom}>Masukkan Keranjang</Text>
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    </View>
  );
};

export default FormPemesanan;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    height: 56,
    width: '100%',
    backgroundColor: 'white',
    // justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 22,
    shadowColor: 'black',
    shadowOffset: {
      height: 2,
      width: 0,
    },
    shadowOpacity: 0.25,
    elevation: 5,
  },
  tittleInput: {
    color: '#BB2427',
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: 25,
  },
  textInput: {
    backgroundColor: '#F6F8FF',
    // position: 'absolute',
    width: '100%',
    height: 45,
    marginTop: 11,
    paddingLeft: 10,
    borderRadius: 10,
  },
  inputImg: {
    backgroundColor: 'white',
    borderRadius: 10,
    height: 84,
    width: 84,
    // justifyContent: 'center',
    alignItems: 'center',
    marginTop: 14,
    marginBottom: 40,
    borderColor: '#BB2427',
    borderWidth: 1,
  },
  catatan: {
    backgroundColor: '#F6F8FF',
    // position: 'absolute',
    width: '100%',
    height: 100,
    marginTop: 11,
    paddingLeft: 10,
    borderRadius: 10,
  },
  bottomconfirm: {
    marginTop: 30,
    backgroundColor: '#BB2427',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    paddingVertical: 15,
  },
  textbottom: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },
});
